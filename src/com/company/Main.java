package com.company;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        ArrayList<String> fileLines = new ArrayList<String>();
        ArrayList<Character> fileChars = new ArrayList<Character>();

        String filename = "test.java";
        Path path = Paths.get(filename);
        Scanner scanner = new Scanner(path);
        File file = new File(filename);
        if (!file.exists() || !file.isFile()) return;

        double fileSizeInKb = (double)file.length() / 1024.0;
        //System.out.println("Read text file using Scanner");
        //read line by line
        while(scanner.hasNextLine()){
            //process each line
            fileLines.add(scanner.nextLine());
        }


        //fileLines.forEach( line -> System.out.println(line));
        fileChars.forEach( c -> System.out.println(c));



        Charset encoding = Charset.defaultCharset();
        /*for (String filename : args) {
            File file = new File(filename);
            handleFile(file, encoding);
        }*/
        handleFile(file, encoding);
    }

    private static void handleFile(File file, Charset encoding)
            throws IOException {
        try (InputStream in = new FileInputStream(file);
             Reader reader = new InputStreamReader(in, encoding);
             // buffer for efficiency
             Reader buffer = new BufferedReader(reader)) {
            handleCharacters(buffer);
        }
    }

    private static void handleCharacters(Reader reader)
            throws IOException {
        ArrayList<Character> fileChars = new ArrayList<Character>();
        int r;
        while ((r = reader.read()) != -1) {
            char ch = (char) r;
            //System.out.println("Do something with " + ch);
            fileChars.add(ch);
        }

        HashMap<Character, Character> brackets = new HashMap<>();
        brackets.put('(', ')');
        brackets.put('[', ']');
        brackets.put('{', '}');

        HashMap<Character, Character> oppositeBrackets = new HashMap<>();
        oppositeBrackets.put(')', '(');
        oppositeBrackets.put(']', '[');
        oppositeBrackets.put('}', '{');

        HashMap<Character, Integer> bracketCounts = new HashMap<>();
        bracketCounts.put('(', 0);
        bracketCounts.put('[', 0);
        bracketCounts.put('{', 0);
        bracketCounts.put(')', 0);
        bracketCounts.put(']', 0);
        bracketCounts.put('}', 0);

        int bl=0, cl=0, sl=0,br=0, cr=0, sr=0;
        Character lastExpanding=' ';
        ArrayList<Character> expands = new ArrayList<Character>();
        for (Character c : fileChars)
        {
            if (brackets.containsKey(c)) {
                expands.add(c);
            }
            if (oppositeBrackets.containsKey(c) && expands.size() > 0) {
                System.out.println(c);

                expands.remove(expands.size()-1);
                if (expands.get(expands.size()-1) != oppositeBrackets.get(c)) {
                    System.out.println("Last expanding is: " + expands.get(expands.size()-1));
                    System.out.println("ERROR!! ()");
                }

            }

            if (bracketCounts.containsKey(c)) {
                bracketCounts.put( c, bracketCounts.get(c) + 1 );
            }
            /*
            System.out.println(c);
            if (c == '(') {
                System.out.println("Setting last expanding to: " + c);
                expands.add(c);
                lastExpanding = c;
                bl++;
            }
            if (c == ')')  {
                if (expands.get(expands.size()-1) != '(') {
                    System.out.println("Last expanding is: " + expands.get(expands.size()-1));
                    System.out.println("ERROR!! ()");
                    break;
                }
                expands.remove(expands.size()-1);
                br++;
            }

            if (c == '{')  {
                System.out.println("Setting last expanding to: " + c);
                expands.add(c);
                lastExpanding = c;
                cl++;
            }
            if (c == '}')  {
                if (expands.get(expands.size()-1) != '{') {
                    System.out.println("Last expanding is: " + expands.get(expands.size()-1));
                    System.out.println("ERROR!! {}");
                    break;
                }
                expands.remove(expands.size()-1);
                cr++;
            }
            if (c == '[')  {
                System.out.println("Setting last expanding to: " + c);
                expands.add(c);
                lastExpanding = c;
                sl++;
            }
            if (c == ']')  {
                if (expands.get(expands.size()-1) != '[') {
                    System.out.println("Last expanding is: " + expands.get(expands.size()-1));
                    System.out.println("ERROR!! []");
                    break;
                }
                expands.remove(expands.size()-1);
                sr++;
            }

             */

        }

        for(Map.Entry<Character, Integer> entry : bracketCounts.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}
